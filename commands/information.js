'use strict';

const Discord = require('discord.js');

exports.info = {
    command: '.information',
    help: {
        command: '.information <treść>',
      },
      aliases: [
          '.info',
          '.information'
      ]
  };

exports.function = async (parameters) => {
    const args = parameters.args;
    const config = parameters.config;
    const message = parameters.message;

    if(!message.guild) {
        await message.channel.send('Ta komenda jest dostępna tylko na serwerach!');
    } else {
        if(!message.member.hasPermission('MANAGE_MESSAGES')) {
        } else {
            const content = args.slice(1).join(' ');

            if(!content) {
                await message.reply('Correct use: `.info <treść>`!');
            } else {
                await message.delete();

                const embed = new Discord.RichEmbed();
                embed.setAuthor('Information', message.client.user.displayAvatarURL);
                embed.setColor(`#747d8c`);
                embed.setDescription(content);
                embed.setFooter(message.member.displayName, message.author.displayAvatarURL);

                const newMessage = await message.channel.send(embed);
                }
        }
    }
};
