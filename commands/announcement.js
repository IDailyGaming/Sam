'use strict';

const Discord = require('discord.js');

exports.info = {
    command: '.announcement',
    help: {
        command: '.announcement <treść>',
      },
      aliases: [
          '.anno'
      ]
  };

exports.function = async (parameters) => {
    const args = parameters.args;
    const config = parameters.config;
    const message = parameters.message;

    if(!message.guild) {
        await message.channel.send('Ta komenda jest dostępna tylko na serwerach!');
    } else {
        if(!message.member.hasPermission('MANAGE_MESSAGES')) {
            await message.reply('You are not authorized! Your rank is too low to use this command.');
        } else {
            const content = args.slice(1).join(' ');

            if(!content) {
                await message.reply('Correct use: `.announcement <treść>`!');
            } else {
                await message.delete();

                const embed = new Discord.RichEmbed();
                embed.setAuthor('Announcement', "https://i.imgur.com/1rMuqgm.png");
                embed.setColor('#ff4757');
                embed.setDescription(content);
                embed.setFooter(message.member.displayName, message.author.displayAvatarURL);

                const newMessage = await message.channel.send(embed);

                for(const reaction of config.commands.announcement.reactions) {
                    await newMessage.react(reaction);
                }
            }
        }
    }
};
